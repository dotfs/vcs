# dotfs

This is my collection of dotfiles for various Linux tools and utilities. The 
whole thing is being tracked with `git` using `vcsh` for multiple repository 
tracking.  Each repository in this group is responsible for one logical 
component that can be mix-and-matched in a modular way to hopefully support 
systems with or without a window system, for example.

## Dependencies

The following packages are required to boostrap the installation process with 
which the rest of this document outlines. Currently, the primary system this 
was tested on is `Linux 5.7.8-arch1-1 x86_64`.

- `git` v2.27.0
- `vcsh` v1.20151229
- `myreposa` v1.20180726
- `zsh` 5.8

## Quickstart

### TLDR

```bash
# Install vcsh if needed
# Install mr if needed
# Install direnv if needed
bash <(curl -s https://gitlab.com/-/snippets/1999688/raw)
```

### Description

The installation procedure consists of cloning the entrypoint repository, 
symbolic linking the desired repository configurations and finally fetching the 
approriate contents using `myrepos` on top of `vcsh`. Finally a cleanup of 
leftover files is performed.

```bash
vcsh clone git@gitlab.com:dotfs/vcs.git
export XDG_CONFIG_HOME=${XDG_CONFIG_HOME:-$HOME/.config}
mkdir ${XDG_CONFIG_HOME}/mr/config.d
pushd $!
for repo in ../available.d/*; do
  echo -n ":: Include ${repo}? [Y/n] "
  read REPLY
  echo
  if [[ $REPLY =~ ^[Yy]$ ]]; then
    echo ln -s $repo .a && ln -s $repo .
  else
    echo rm -f $(basename $repo) && rm -f $(basename $repo)
  fi
done
popd
mr --trust-all --config ${XDG_CONFIG_HOME}/mr/mrconfig update
!! # Hack to workaround "fatal: Non-fast-forward commit ..."
for repo in $(vcsh list); do
  vcsh $repo config status.showuntrackedfiles no
done
exec zsh
```

## Installation

The operating system is Arch Linux for its rolling releases and Arch User 
Repository (AUC), which contains most 3rd party applications. The following 
list details the packages to be installed via the package manager --- pacman.

- pacstrap: base base-devel linux linux-firmware intel-ucode
- pacman:   gvim netctl wpa_supplicant dhcpcd dialog xf86-video-intel

The following list details the packages used in a desktop environment.

- version control: openssh git
- display server: bspwm sxhkd xorg-server xorg-xinit xorg-xsetroot
- shell: zsh tmux
- man pages: man-db man-pages
- permission: sudo
- file viewer: mpv sxiv zathura zathura-pdf-mupdf
- desktop utilities:
  - sound: alsa-utils
  - keyboard: xclip xcape xorg-xset
- laptop utilities:
  - laptop power management: tlp
  - trackpad: xorg-xinput xf86-input-libinput
  - battery: acpi
  - backlight: xorg-xbacklight

### Configuration

This configuration should be performed once after the packages from the 
previous section were installed succesfully.

```bash
# Install AUR helper
git clone https://aur.archlinux.org/yay.git pkg/yay
(cd pkg/yay && makepkg -si)
# Configure laptop power management
systemctl enable --now tlp tlp-sleep
# Configure wireless connectivity device
systemctl enable --now netctl-auto@$(printf '%s\n' /sys/class/net/*/wireless | awk -F'/' '{ print $5 }')
# Configure ssh private key
ssh-keygen -t ed25519
```

### Utilities

The following are utilities applications that enhance the workflow inside this 
environment. The AUR helper --- `yay` --- is used to install them.

- wget
- rclone
- ranger w3m ffmpegthumbnailer
- ripgrep
- exa
- fzf
- fd
- rtorrent
- wireguard-tools
- vivaldi vivaldi-ffmpeg-codecs pepper-flash
- bdf-tewi-git
- gotop
- iftop
- ncdu
- direnv
- tmsu
- rmlint findimagedupes go-perceptualhash
- mc

### Future ideas

1. `fish` instead of `zsh`
2. `kitty` instead of `st` or `urxvt` for image viewing support
3. day mode and night mode
